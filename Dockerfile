FROM java:8-jdk

ENV app_destination /data/app-store.jar
ENV mongodb.hosts $HOSTNAME:27017

ADD target/app-store.jar $app_destination

EXPOSE 8080

CMD java -jar $app_destination