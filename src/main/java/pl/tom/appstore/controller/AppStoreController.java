package pl.tom.appstore.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import pl.tom.appstore.api.ASApp;
import pl.tom.appstore.api.ASResponse;
import pl.tom.appstore.api.ASClient;

@Controller
@RequestMapping("/app-store")
public class AppStoreController {

	@Autowired
	ASClient appStoreClient;

	@RequestMapping("/search")
	public List<ASApp> search(@RequestParam String term) {
		ASResponse response = appStoreClient.get(term);
		return response.getResults();
	}
}
