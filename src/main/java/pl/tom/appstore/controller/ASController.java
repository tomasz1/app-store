package pl.tom.appstore.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.tom.appstore.api.ASClient;
import pl.tom.appstore.api.ASRequest;
import pl.tom.appstore.api.ASResponse;
import pl.tom.appstore.command.TopRankings;

@Controller
@RequestMapping("/as-api")
public class ASController {

	private final static Logger log = LoggerFactory.getLogger(ASController.class);

	@Autowired
	ASClient asClient;

	@RequestMapping(value = "/search", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	ASResponse search(ASRequest request) {

		log.info("search()");

		return asClient.get(request);
	}

	@RequestMapping(value = "/top-rankings", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	TopRankings topRankings(ASRequest request) {

		request.setEntity("");
		ASResponse iPhoneRanking = asClient.get(request);

		request.setEntity("iPadSoftware");
		ASResponse iPadRanking = asClient.get(request);

		TopRankings topRankings = new TopRankings();
		topRankings.setiPhoneRanking(iPhoneRanking.getResults());
		topRankings.setiPadRanking(iPadRanking.getResults());

		return topRankings;
	}

}
