package pl.tom.appstore.controller;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.tom.appstore.AjaxUtils;
import pl.tom.appstore.SessionHelper;
import pl.tom.appstore.WebKeys;
import pl.tom.appstore.api.RankService;
import pl.tom.appstore.command.AjaxResponse;
import pl.tom.appstore.command.TermRanking;
import pl.tom.appstore.model.App;
import pl.tom.appstore.model.AppTerm;
import pl.tom.appstore.service.AppService;
import pl.tom.appstore.service.AppTermService;

@Controller
@RequestMapping("/search-ranking")
public class SearchRankingController {

	private final static Logger log = LoggerFactory.getLogger(SearchRankingController.class);

	@Autowired
	AppTermService appTermService;

	@Autowired
	AppService appService;

	@Autowired
	SessionHelper sessionHelper;

	@Autowired
	RankService rankService;

	@RequestMapping(method = RequestMethod.GET)
	public String view(Model model, HttpSession session) {

		App app = sessionHelper.getCurrentApp(session);
		if (app != null) {
			List<TermRanking> termRankings = rankService.getTermRankingsForAppTerms(appTermService.getAppAppTerms(app),
					app);
			ObjectMapper mapper = new ObjectMapper();
			try {
				model.addAttribute("initialTermRankings", mapper.writeValueAsString(termRankings));
			} catch (Exception e) {
				log.error("Can't write JSON", e);
			}
		}
		return "content.search-ranking";
	}

	@RequestMapping(value = "/get-term-rankings", method = RequestMethod.GET)
	public @ResponseBody
	List<TermRanking> getTermRankings(HttpSession session) {

		List<TermRanking> termRankings = Collections.emptyList();

		App app = sessionHelper.getCurrentApp(session);
		if (app != null) {
			termRankings = rankService.getTermRankingsForAppTerms(appTermService.getAppAppTerms(app), app);
		}

		return termRankings;
	}

	@RequestMapping(value = "/get-term-ranking", method = RequestMethod.GET)
	public @ResponseBody
	TermRanking getTermRanking(@RequestParam String term, HttpSession session) {

		TermRanking termRanking = null;
		App app = sessionHelper.getCurrentApp(session);
		if (app != null) {
			termRanking = rankService.getTermRanking(term, app);
		}

		return termRanking;
	}

	@RequestMapping(value = "/add-keyword", method = RequestMethod.POST)
	public @ResponseBody
	AjaxResponse addKeyword(@RequestParam(required = false) String keyword, HttpSession session) {

		if (StringUtils.isBlank(keyword)) {
			return AjaxUtils.errorResponse("Keyword is empty");
		}

		App app = (App) session.getAttribute(WebKeys.CURRENT_APP);

		if (app != null) {
			List<AppTerm> appTerms = appTermService.getAppAppTerms(app);
			if (CollectionUtils.isNotEmpty(appTerms)) {
				for (AppTerm appTerm : appTerms) {
					if (appTerm.getTerm().equalsIgnoreCase(keyword)) {
						return AjaxUtils.errorResponse("Keyword already exists");
					}
				}
			}
			AppTerm newTerm = new AppTerm();
			newTerm.setApp(app);
			newTerm.setTerm(keyword);

			newTerm = appTermService.add(newTerm);

			sessionHelper.updateCurrentApp(session);
			return AjaxUtils.okResponse();

		} else {
			return AjaxUtils.errorResponse("App Not Found");
		}
	}
}
