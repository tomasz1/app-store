package pl.tom.appstore.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.tom.appstore.AjaxUtils;
import pl.tom.appstore.SessionHelper;
import pl.tom.appstore.UserUtils;
import pl.tom.appstore.command.AjaxResponse;
import pl.tom.appstore.model.App;
import pl.tom.appstore.model.User;
import pl.tom.appstore.service.AppService;

@Component
@RequestMapping("/app")
public class CurrentAppController {
	private final static Logger log = LoggerFactory.getLogger(CurrentAppController.class);

	@Autowired
	AppService appService;

	@Autowired
	SessionHelper sessionHelper;

	@RequestMapping(value = "/change-app", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody
	AjaxResponse changeApp(@RequestParam String appId, Model model, HttpServletResponse httpServletResponse,
			HttpSession session) {

		AjaxResponse ajaxResponse = null;
		try {
			User user = UserUtils.getUser();
			App app = appService.findByAppIdAndUser(appId, user);

			log.debug("User {} change currentApp {} to {}", session.getAttribute("currentApp"), user, app);

			if (app != null) {

				sessionHelper.updateCurrentApp(session);

				return AjaxUtils.okResponse();
			}

			ajaxResponse = AjaxUtils.errorResponse("App Not Found");

		} catch (Exception e) {

			log.error("AddApp", e);

			ajaxResponse = AjaxUtils.errorResponse("Error");
			httpServletResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		return ajaxResponse;
	}

	@RequestMapping(value = "/change-lang", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody
	AjaxResponse changeLang(@RequestParam String appLang, Model model, HttpServletResponse httpServletResponse,
			HttpSession session) {

		log.debug("Lang change {} to {}", session.getAttribute("currentAppLang"), appLang);

		session.setAttribute("currentAppLang", appLang);
		return AjaxUtils.okResponse();
	}
}
