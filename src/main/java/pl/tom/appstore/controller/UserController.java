package pl.tom.appstore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.tom.appstore.service.UserService;

@Controller
public class UserController {

	@Autowired
	UserService userService;

	@RequestMapping("/user/{id}")
	public String user(@PathVariable long id, Model model) {

		model.addAttribute("user", userService.get(id));

		return "user";
	}
}
