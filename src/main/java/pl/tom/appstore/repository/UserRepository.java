package pl.tom.appstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.tom.appstore.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	public User findByUsername(String username);
}
