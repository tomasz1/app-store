package pl.tom.appstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.tom.appstore.model.App;
import pl.tom.appstore.model.AppTerm;

public interface AppTermRepository extends JpaRepository<AppTerm, Long> {

	public List<AppTerm> findByApp(App app);
}
