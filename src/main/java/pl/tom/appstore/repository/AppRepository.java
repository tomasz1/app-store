package pl.tom.appstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.tom.appstore.model.App;
import pl.tom.appstore.model.User;

public interface AppRepository extends JpaRepository<App, Long> {

	public List<App> findAppByUserId(long userId);

	public List<App> findAppByUser(User user);

	public App findByAppIdAndUser(String appId, User user);
}
