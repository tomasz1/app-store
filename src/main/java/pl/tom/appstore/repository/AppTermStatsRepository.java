package pl.tom.appstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.tom.appstore.model.AppTermStats;

public interface AppTermStatsRepository extends JpaRepository<AppTermStats, Long> {

}
