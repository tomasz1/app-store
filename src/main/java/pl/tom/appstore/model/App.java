package pl.tom.appstore.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * @author tomek
 * 
 */
@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "id", "user_id" }) })
public class App implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String appId;
	private String name;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@ManyToOne
	private User user;

	@OneToMany(mappedBy = "app", cascade = { CascadeType.REMOVE, CascadeType.PERSIST })
	private List<AppTerm> keywords = null;

	private boolean active = true;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public List<AppTerm> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<AppTerm> keywords) {
		this.keywords = keywords;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "App [id=" + id + ", appId=" + appId + ", name=" + name + ", createDate=" + createDate + ", user="
				+ user + ", active=" + active + "]";
	}

}
