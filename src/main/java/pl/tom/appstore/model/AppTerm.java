package pl.tom.appstore.model;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class AppTerm implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String term;

	@ManyToOne(optional = false)
	@JoinColumn(name = "app_id")
	private App app;

	@OneToMany(mappedBy = "appKeyword")
	private Collection<AppTermStats> appTermStats;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public App getApp() {
		return app;
	}

	public void setApp(App app) {
		this.app = app;
	}

	public Collection<AppTermStats> getAppTermStats() {
		return appTermStats;
	}

	public void setAppTermStats(Collection<AppTermStats> appTermStats) {
		this.appTermStats = appTermStats;
	}

}
