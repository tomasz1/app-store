package pl.tom.appstore;

public class WebKeys {

	public static final String USER_APPS = "userApps";
	public static final String CURRENT_APP = "currentApp";
	public static final String CURRENT_APP_LANG = "currentAppLANG";
	public static final String TERM_RANKS = "termRanks";
}
