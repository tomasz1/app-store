package pl.tom.appstore;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import pl.tom.appstore.model.App;
import pl.tom.appstore.model.User;
import pl.tom.appstore.service.AppService;

public class SessionHelper {

	@Autowired
	AppService appService;

	public void updateCurrentApp(HttpSession session) {
		App app = (App) session.getAttribute(WebKeys.CURRENT_APP);

		if (app != null) {
			setCurrentApp(appService.get(app.getId()), session);
		}
	}

	public void setCurrentApp(App app, HttpSession session) {
		session.setAttribute(WebKeys.CURRENT_APP, app);
	}

	public App getCurrentApp(HttpSession session) {
		return (App) session.getAttribute(WebKeys.CURRENT_APP);
	}

	public void updateUserApps(HttpSession session) {
		User user = UserUtils.getUser();
		session.setAttribute(WebKeys.USER_APPS, appService.getUserApps(user));
	}
}
