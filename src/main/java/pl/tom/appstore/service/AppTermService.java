package pl.tom.appstore.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.tom.appstore.model.App;
import pl.tom.appstore.model.AppTerm;
import pl.tom.appstore.repository.AppTermRepository;

@Service
public class AppTermService {

	@Autowired
	AppTermRepository appTermRepository;

	public AppTerm add(AppTerm appKeyword) {
		return appTermRepository.save(appKeyword);
	}

	public List<AppTerm> getAppAppTerms(App app) {
		return appTermRepository.findByApp(app);
	}
}
