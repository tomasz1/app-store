package pl.tom.appstore.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.tom.appstore.api.ASApp;
import pl.tom.appstore.model.App;
import pl.tom.appstore.model.User;
import pl.tom.appstore.repository.AppRepository;

@Service
public class AppService {

	@Autowired
	AppRepository appRepository;

	public App get(long id) {
		return appRepository.findOne(id);
	}

	public List<App> getUserApps(long userId) {
		return appRepository.findAppByUserId(userId);
	}

	public List<App> getUserApps(User user) {
		return appRepository.findAppByUser(user);
	}

	public App findByAppIdAndUser(String appId, User user) {
		return appRepository.findByAppIdAndUser(appId, user);
	}

	public App addApp(ASApp asApp, User user) {
		App app = new App();
		app.setAppId(asApp.getTrackId());
		app.setUser(user);
		app.setCreateDate(new Date());
		app.setActive(true);
		app.setName(asApp.getTrackName());

		app = appRepository.save(app);

		return app;
	}
}
