package pl.tom.appstore.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import pl.tom.appstore.model.User;
import pl.tom.appstore.service.UserService;

public class UserDetailsServiceImpl implements UserDetailsService {

	private final static Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

	UserService userService;

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = null;
		try {
			user = userService.findByUsername(username);
		} catch (Exception e) {
			log.error("Unable to find user", e);
		}

		if (user == null) {
			throw new UsernameNotFoundException("User Not Found");
		}
		return user;
	}

}
