package pl.tom.appstore.api;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ASResponse {

	private String term;
	private long resultCount;
	private List<ASApp> results;

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public long getResultCount() {
		return resultCount;
	}

	public void setResultCount(long resultCount) {
		this.resultCount = resultCount;
	}

	public List<ASApp> getResults() {
		return results;
	}

	public void setResults(List<ASApp> results) {
		this.results = results;
	}

	@Override
	public String toString() {
		return "ASResponse [resultCount=" + resultCount + ", results=" + results + "]";
	}

}
