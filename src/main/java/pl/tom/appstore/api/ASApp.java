package pl.tom.appstore.api;

import java.util.Arrays;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ASApp {

	private String bundleId;
	private String trackId;
	private String trackName;
	private String artistId;
	private String artistName;

	private String kind;
	private String thumb;
	private String description;
	private String[] genres;
	private long[] genresIds;
	private String price;
	private String artworkUrl60;
	private String formattedPrice;

	public String getBundleId() {
		return bundleId;
	}

	public void setBundleId(String bundleId) {
		this.bundleId = bundleId;
	}

	public String getTrackId() {
		return trackId;
	}

	public void setTrackId(String trackId) {
		this.trackId = trackId;
	}

	public String getTrackName() {
		return trackName;
	}

	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}

	public String getArtistId() {
		return artistId;
	}

	public void setArtistId(String artistId) {
		this.artistId = artistId;
	}

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getThumb() {
		return thumb;
	}

	public void setThumb(String thumb) {
		this.thumb = thumb;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String[] getGenres() {
		return genres;
	}

	public void setGenres(String[] genres) {
		this.genres = genres;
	}

	public long[] getGenresIds() {
		return genresIds;
	}

	public void setGenresIds(long[] genresIds) {
		this.genresIds = genresIds;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "ASApp [bundleId=" + bundleId + ", trackId=" + trackId + ", trackName=" + trackName + ", artistId="
				+ artistId + ", artistName=" + artistName + ", kind=" + kind + ", thumb=" + thumb + ", description="
				+ description + ", genres=" + Arrays.toString(genres) + ", genresIds=" + Arrays.toString(genresIds)
				+ "]";
	}

	public String getArtworkUrl60() {
		return artworkUrl60;
	}

	public void setArtworkUrl60(String artworkUrl60) {
		this.artworkUrl60 = artworkUrl60;
	}

	public String getFormattedPrice() {
		return formattedPrice;
	}

	public void setFormattedPrice(String formattedPrice) {
		this.formattedPrice = formattedPrice;
	}

}
