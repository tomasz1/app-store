package pl.tom.appstore.command;

import java.util.List;

import pl.tom.appstore.api.ASApp;

public class TopRankings {
	List<ASApp> iPhoneRanking = null;
	List<ASApp> iPadRanking = null;

	public List<ASApp> getiPhoneRanking() {
		return iPhoneRanking;
	}

	public void setiPhoneRanking(List<ASApp> iPhoneRanking) {
		this.iPhoneRanking = iPhoneRanking;
	}

	public List<ASApp> getiPadRanking() {
		return iPadRanking;
	}

	public void setiPadRanking(List<ASApp> iPadRanking) {
		this.iPadRanking = iPadRanking;
	}

}
