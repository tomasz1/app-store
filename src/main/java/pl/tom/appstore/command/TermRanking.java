package pl.tom.appstore.command;

public class TermRanking {

	private String keywords;
	private double traffic;
	private double iPhoneDifficulty;
	private double iPadDifficulty;
	private int iPhoneApps;
	private int iPadApps;
	private int iPhoneRank;
	private int iPadRank;

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public double getTraffic() {
		return traffic;
	}

	public void setTraffic(double traffic) {
		this.traffic = traffic;
	}

	public double getiPhoneDifficulty() {
		return iPhoneDifficulty;
	}

	public void setiPhoneDifficulty(double iPhoneDifficulty) {
		this.iPhoneDifficulty = iPhoneDifficulty;
	}

	public double getiPadDifficulty() {
		return iPadDifficulty;
	}

	public void setiPadDifficulty(double iPadDifficulty) {
		this.iPadDifficulty = iPadDifficulty;
	}

	public int getiPhoneApps() {
		return iPhoneApps;
	}

	public void setiPhoneApps(int iPhoneApps) {
		this.iPhoneApps = iPhoneApps;
	}

	public int getiPadApps() {
		return iPadApps;
	}

	public void setiPadApps(int iPadApps) {
		this.iPadApps = iPadApps;
	}

	public int getiPhoneRank() {
		return iPhoneRank;
	}

	public void setiPhoneRank(int iPhoneRank) {
		this.iPhoneRank = iPhoneRank;
	}

	public int getiPadRank() {
		return iPadRank;
	}

	public void setiPadRank(int iPadRank) {
		this.iPadRank = iPadRank;
	}

}
