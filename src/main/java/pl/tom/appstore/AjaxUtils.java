package pl.tom.appstore;

import pl.tom.appstore.command.AjaxResponse;

public class AjaxUtils {

	public static AjaxResponse errorResponse(String message) {
		return new AjaxResponse(message, true);
	}

	public static AjaxResponse successResponse(String message) {
		return new AjaxResponse(message);
	}

	public static AjaxResponse okResponse() {
		return new AjaxResponse("OK");
	}
}
