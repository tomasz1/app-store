<%@ include file="/WEB-INF/includes/taglibs.jsp"%>

<div class="row-fluid">
	<div class="span9">
		<table id="table-keywords" class="table table-hover table-striped tablesorter">
			<thead>
				<tr>
					<th><spring:message code="keywords.search.term" /></th>
					<th><spring:message code="keywords.traffic" /></th>
					<th><spring:message code="keywords.iphone.difficulty" /></th>
					<th><spring:message code="keywords.ipad.difficulty" /></th>
					<th><spring:message code="keywords.iphone.apps" /></th>
					<th><spring:message code="keywords.ipad.apps" /></th>
					<th><spring:message code="keywords.iphone.rank" /></th>
					<th><spring:message code="keywords.ipad.rank" /></th>
				</tr>
			</thead>
			<tbody data-bind="foreach: termRankings" >
					<tr data-bind="click: \$parent.updateUI">
						<td data-bind="text: keywords" ></td>
						<td data-bind="text: traffic" ></td>
						<td data-bind="text: iPhoneDifficulty" ></td>
						<td data-bind="text: iPadDifficulty" ></td>
						<td data-bind="text: iPhoneApps" ></td>
						<td data-bind="text: iPadApps" ></td>
						<td data-bind="text: iPhoneRank" ></td>
						<td data-bind="text: iPadRank" ></td>
						<td data-bind="click: \$parent.removeTerm, clickBubble: false"><button class="btn btn-mini" type="button"><i class="icon-trash icon-blue"></i></button></td>
					</tr>
			</tbody>
		</table>

		<a href="#" data-target="#add-keyword" class="btn btn-primary" data-toggle="modal">
			<i class="icon-plus icon-white"></i> <spring:message code="keywords.add.new.keyword" />
		</a>

		<div class="modal hide" id="add-keyword" tabindex="-1">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>
					<spring:message code="keywords.add.new.keyword.for" arguments="${currentApp.name }" />
				</h3>
			</div>
			<div class="modal-body">
				<form:form id="addKeywordForm" action="/add-keyword" method="POST">
					<input id="keyword" type="text" />
				</form:form>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal">
					<spring:message code="button.close" />
				</button>
				<button class="btn btn-primary">
					<spring:message code="button.choose" />
				</button>
			</div>
		</div>
	</div>

	<div class="span3">
		<ul class="nav nav-tabs">
	 		<li class="active" ><a href="#iPhone-tab" data-toggle="tab" >iPhone</a></li>
			<li><a href="#iPad-tab" data-toggle="tab" >iPad</a></li>
	 	</ul>
		<div id="top-rank" class="tab-content" >
		
			<div class="tab-pane active" id="iPhone-tab">
				<div data-bind="visible: iPhoneRanking().length == 0">
					<spring:message code="keywords.no.iphone.apps.for.keyword" /> <span class="label label-success" data-bind="text: term" ></span>
				</div>
	
				<table class="table table-hover" data-bind="visible: iPhoneRanking().length > 0" >
					<thead>
						<tr>
							<td>#</td>
							<td><spring:message code="keywords.top.apps.for" /> <span class="label label-success" data-bind="text: term"></span></td>
						</tr>
					</thead>
					<tbody  data-bind="foreach: iPhoneRanking">
						<tr>
							<td data-bind="text: $root.itemIndex($index())" ></td>
							<td ><img data-bind="attr: { src: artworkUrl60, alt: name }" /></td>
							<td data-bind="text: trackName" ></td>
							<td><span class="label label-info" data-bind="text: formattedPrice" ></span></td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div class="tab-pane" id="iPad-tab">
				<div data-bind="visible: iPadRanking().length == 0">
					<spring:message code="keywords.no.ipad.apps.for.keyword" /> <span class="label label-success" data-bind="text: term" ></span>
				</div>
	
				<table class="table table-hover" data-bind="visible: iPadRanking().length > 0" >
					<thead>
						<tr>
							<td>#</td>
							<td><spring:message code="keywords.top.apps.for" /> <span class="label label-success" data-bind="text: term"></span></td>
						</tr>
					</thead>
					<tbody  data-bind="foreach: iPadRanking">
						<tr>
							<td data-bind="text: $root.itemIndex($index())" ></td>
							<td ><img data-bind="attr: { src: artworkUrl60, alt: name }" /></td>
							<td data-bind="text: trackName" ></td>
							<td><span class="label label-info" data-bind="text: formattedPrice" ></span></td>
						</tr>
					</tbody>
				</table>
			</div>
			
		</div>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

	function SearchApp() {
	    var self = this;
	    self.term = ko.observable();
	    self.entity = ko.observable();
	    self.iPhoneRanking = ko.observableArray(); 
	    self.iPadRanking = ko.observableArray(); 

	    self.itemIndex = function(index) {
	        return index + 1;
	    };
	    
	    var initialTermRanking = '${initialTermRankings}';
	    if (initialTermRanking.length > 0) {
			initialTermRanking = $.parseJSON(initialTermRanking);
	    } else {
			initialTermRanking = [];
	    }
	    self.termRankings = ko.observableArray(initialTermRanking);
	    
		ko.computed(function() {
				var data =  {
				    limit: 10,
					term: self.term(),
				};
				$.getJSON('<c:url value="/as-api/top-rankings" />', data, function(data) {
						self.iPhoneRanking(data.iPhoneRanking);
						self.iPadRanking(data.iPadRanking);
				})
				.fail( function() {
					self.iPhoneRanking([]);
					self.iPadRanking([]);
					$.bootstrapGrowl('Error');
					console.log('Error');
				});
	    });
	    
	    self.updateUI = function(termRanking){
			self.term(termRanking.keywords);
	    };
	    
	    self.removeTerm = function(termRanking){
			console.log(termRanking);
			self.termRankings.remove(termRanking);
	    };


	};
	
	ko.applyBindings(new SearchApp());

	$('#table-keywords').tablesorter();

	function addKeyword() {
	    $.ajax({
		url : '<c:url value="/search-ranking/add-keyword" />',
		type : 'POST',
		dataType : 'json',
		data : {
		    keyword : $('#keyword').val()
		},
		success : function(data) {
		    if (!data.error) {
				window.location.reload();
		    } else {
				$.bootstrapGrowl(data.message);
		    }
		},
		error : function() {
		    $.bootstrapGrowl('Error');
		    console.log('Error');
		}
	    });
	}

	$('#add-keyword .btn-primary').on('click', function() {
	    addKeyword();
	});

	$('#addKeywordForm').on('submit', function() {
	    addKeyword();
	    return false;
	});

    });
</script>