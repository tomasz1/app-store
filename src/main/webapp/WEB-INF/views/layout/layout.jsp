<%@ include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" /></title>

<link rel="stylesheet" type="text/css" href="<c:url value='/resources/bootstrap/css/bootstrap.css'/>" />
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/main.css'/>" />
<script src="http://code.jquery.com/jquery-1.10.0.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.min.js"></script>
<script type="text/javascript" src="<c:url value='/resources/bootstrap/js/bootstrap.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/bootstrap/js/jquery.bootstrap-growl.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/jquery.tablesorter.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/jquery.tmpl.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/knockout-2.2.1.js'/>"></script>

</head>
<body>
	<div class="container-fluid">
		<tiles:insertAttribute name="top" />

		<tiles:insertAttribute name="body" />

		<tiles:insertAttribute name="footer" />
	</div>
</body>
</html>